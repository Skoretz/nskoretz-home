---
title: Goals and Reflection
---

# Goals and Reflection

As part of the co-op process at the University of Guelph, students are required to create goals.
These goals can cover soft or hard skills, but students are required to track progress and set
small milestones for the goals, which is a really great skill. Below are the goals that I set for
myself, how they evolved, and my progress towards them.

## Goal 1

**Get familiar with Splunk. Learn how it works and create useful Dashboards that can help myself
and my team with the work we're doing.**

*Type* : LITERACY - Technological Literacy

<details>

### Action Plan

- Get access to Splunk
- Read up on how it works, and the syntax of searches
- Breakdown other dashboards that people have written to understand what they do
- Create my own searches and dashboards that relate to my automation projects

### Measure of Success

Create 2 Dashboards that monitor logs for information on my automation playbooks.

### Reflection

As time progressed in the semester I was engaged more with Ansible Automation projects than Splunk
projects. As such, I did not get the change to explore Splunk as much as I would have liked.

</details>

## Goal 2

**Dive deeper into GitLab-CI. Learn how to use GitLab Runners effectively to set-up
dev-testing-staging-prod environments with continuous deployment promoting code once it passes
tests.**

*Type* : LITERACY - Technological Literacy

<details>

### Action Plan

- Each new GitLab project that I create going forward should have dev, testing, staging, prod /
master branches.
- Refactor existing projects to follow that structure
- Read up on automated code promotion through GitLab-CI
- Design tests that can be run in a test harness before promotion to ensure that bugs are not
getting promoted

### Measure of Success

Implement CI automation in at least four projects that I work on.

### Reflection

This was also a goal that I was looking forward to, but sadly could not execute on due to the
companies processes. Just at the tail end of my term did we (the automation team) get approval to
implement Docker and containerization at Symcor. This we're destined to be out testing environments
that would be used in CI, but I won't get a chance to work with them as it was only implemented at
the end of my term. I still feel like I have an okay knowledge level of GitLab-CI, but I would have
appreciated more exposure.

</details>

## Goal 3

**Sharpen skills with Ansible Tower, such as playbook scheduling and workflows. At lot of deeper
functionality remains hidden from me, and I would like to learn how to use Tower to its full
potential.**

*Type* : LITERACY - Technological Literacy

<details>

### Action Plan

- Learn how to use the scheduling tool in Tower using documentation.
- Schedule my own projects, as various ones that I've been assigned so far need to be in the Scheduler eventually.
- Read up on Tower workflows, how they're created, and what their best use cases are
- Conduct a knowledge transfer meeting with others on the Automation Team that can walk me through workflow creation
- Create a workflow for a project.

### Measure of Success

- For the projects that I'm working on that require scheduling, schedule them myself rather than passing that task on to someone up the chain
- Create a useful workflow of playbooks

### Reflection

This goal was definitely fulfilled. I got a lot more experience with Ansible Tower which I
appreciate greatly. The last project I worked on before I left was an end-to-end process
automation involving UIPath for RPA (Robotic Process Automation) and Ansible Playbooks in Ansible
Tower Workflows.

</details>

## Goal 4

**Gain a better understanding of how the automation environment is implemented and why automation
is being used at Symcor.**

*Type* : CRITICAL & CREATIVE THINKING - Depth & Breadth of Understanding

<details>

### Action Plan

Getting a deeper understanding of what tools Symcor has, how they work together now, and what the
end goal of automation is at Symcor. This will let me think about applying automation is different
environments, whether it is a different company, or in my home environment.

### Measure of Success

Write a post on the automation tools and processes at Symcor for my website.

### Reflection

I would say I accomplished this goal. The Automation Team went through a leadership change during
my term and it acted as a bit of a reset for the standards and best practices for automation
projects.

I was able to provide my project templates and personal standards to be the bases of the team-wide
standards. As well, I set up an internal role-sharing repository, which will help reduce
doublework. This lets automation engineers share roles (think of them like functions, just modular
pieces of code) with each other and prevent an engineer writing the same code twice.

I also wrote a good bit of documentation on Ansible Patterns. These are common programming patterns
and how to implement them in Ansible, such as, How to Monitor a Log File, and how to do Try-Catch
error handling.

</details>

## Goal 5

**Gain experience with UIPath and understand the possibilities of Robotic Process Automation (RPA)
at Symcor.**

*Type* : LITERACY - Technological Literacy

<details>

### Action Plan

- Research RPA in general and why it is used over process automation which generally uses back-end
channels
- Read-up on UI path
  - Its strengths and limitations
  - How to use the interface
  - How to create automations
- Gain experience by using UIPath for small automations

### Measure of Success

- If assigned official projects, develop those. Otherwise, if I'm just given time to play around,
create some automated processes that might help me with my personal issue tracking in GitLab.
- Create a technical piece about UIPath and RPA on my website.

### Reflection

Sadly, this goal was another one in which I would have liked to work more on. I did not get a
change to learn much about UIPath, I just worked on projects alongside RPA developers and we also
did integrate certain parts, but I didn't get a full-fledged UIPath / RPA project.

I spent most of my time at Symcor this term developing and implementing Ansible automation projects
from the Automation team funnel. I also had the change to teach / coach other co-ops and
contractors about how to use Ansible. Though, this meant that I got less exposure to other tools
such as UIPath and Splunk.

</details>

---

[Top](#top)
