---
title: Projects
author: Nicholas Skoretz
date: 2019-09-11
sidebarDepth: 3
---

# Projects

I was fortunate enough to obtain a part-time offer from Symcor for the four months that I was in
school and then return for my third co-op term with the Automation Team. The Automation Team is
quite a new team and is in a state of growth right now, so I was able to provide input for
standards that will be used for automation in the future and to be able to write templates and
documentation that will be used extensively.

## Technical Projects

### Symcor Data Exchange (SDX) Server Failover Automation

I talked a little bit about SDX in my previous work-term report where I was working on an
automation piece to onboard client profiles. This time around I was tasked with automation portions
of the server failover process. If a server needs to go into downtime to be upgraded or if there
is some critical failure, the secondary server should come online behind the CNAME and should show
little interruption for the client as possible.

Currently, a failover from a primary to secondary SDX server takes about four hours and members
from three different teams to complete. The "runbook" guide contained 26 or so steps that need to
be performed for a successful failover. As part of this task, I identified the easiest tasks that
could be accomplished and wrote Ansible playbooks for those steps. These playbooks weer migrated
into Ansible Tower, which is an web front-end management interface for Ansible automation pieces.

From Tower we can schedule playbooks, chaining playbooks in workflows, and provide standard
credentials and inventories.

Some of the tasks I automated were:

- SDX Data Synchronization
  - Wrote playbooks to synchronize certain files ad directories from primary to secondary every
  day.
  - For certain files, this automation piece is run every 15 minutes, for others every 4 hours,
  and for the main operating directory it ran every day.
- SDX Start and Stop Services
  - This was a simple automation piece that stopped specific services from running on primary
  before the main failover tasks and started the services on secondary once the server was ready.

### Ad-Hoc CD / DVD Burn Automation

This was quite an interesting project to work on, and it involved a service I didn't even know
Symcor had. As we work closely with banks, we get regular requests to send backup copies of scanned
cheques back to the banks for record keeping. Historically and still today this is done by burning
the images to a CD or DVD and mailing it to the institution.

Based on the fact that we are working with DVDs, this project involved some pretty legacy pieces of
software which we were automating around. This project took up most of my last month and a half at
Symcor, and I worked closely with the RPA team and the DVD and Printing team to create the
automation. This automation was almost end-to-end. We weren't confortable with the automation
interacting directly with the client on first iteration before testing, so there was still a human
layer between the client and the automation piece. But once the request for an ad-hoc DVD request
was approved by the change team, the automation would kick in.

The actual request form was read and parsed with UIPath and then a Service Now Change Request (CR)
was created containing information needed for the burn. From there, a scheduled job in Ansible
Tower queries the Service Now API for CRs that match our criteria. Information is then extracted
from the CR and a workflow template of three playbooks is launched in Ansible Tower.

These playbooks query some databases for client information, create CSV files and XML files that
are to be input into the legacy software, then we take output artifacts of that software and
schedule them so that they can be printed at one of Symcor's two DVD burning sites. From there, the
status of the CR is updated on Service Now and the DVDs will get burned and shipped.

This was an interesting project to work on due to the fact that we had to tie UIPath, Service Now,
and Ansible together using their respective APIs to make a nearly end-to-end automation piece.

### CoE Funnel Report Automation

This was another simple automation project. Remember I mentioned a "funnel" of automation tasks
that have been requested, this funnel has about 400 or so items in it and is sourced from an
associated JIRA board.

The funnel is essentially a spreadsheet that sorts the automation tasks and graphs the charts them
in interesting charts for easier communication. Where previously the JIRA issues were being
imported into this funnel spreadsheet manually, I wrote a quick Ansible playbook that scrapes
issues from the JIRA board using the JIRA API and populates a CSV file with the sanitized return.

Then, that CSV is passed to a separate automation in UIPath that will visually add the contents to
the spreadsheet and generate the graphs.

### Ansible Common Roles and Filters

Of all of the projects that I did, this one might be the one that I am the most proud of. I saw
other automation engineers repeating each other's work due to the fact that they didn't know that
the other had written a role or a filter to accomplish the task already. We also had people working
on automation pieces that weren't part of the automation team, and these individuals in their silos
could benefit from pooling roles and filters together in a shared repository like Ansible Galaxy.

Ansible Galaxy is the official role repository of Ansible, analogous to `npm` or `pip`. Symcor did
not want any internal roles on the public Galaxy, and it doesn't seem like you can self host a
Galaxy instance. Therefore I created an `ansible-common-roles` repository that stored roles that
could be shared and reused by other Automation Engineers and others around the company.

I was able to set the standard for how shared roles should be laid out and how they should be
documented, as well as the process for keeping them up to date on the executing machine. All in all
I set up the common repository and the documentation for using it as well as contributed four roles
and one filter plugin.

## Non-Technical Projects

### Clinic: Learning Ansible

One thing that I appreciate about Symcor is the ability for co-ops to "float" around the company
and try new things that they may be interest them in their spare time. Other co-ops were interested
in the automation that was happening at Symcor, so I set up and ran four "Ansible Clinics" for
other co-op students, teaching them the basics of creating a playbook and working through some
current examples of working automations with them. I also gave them a primer on Ansible Tower.

This was a fun experience that I am grateful to have had, as teaching is quite fun for me and it
really makes me think about how to communicate what I know to others in an understandable and
engaging way.

### Ansible Patterns Documentation

This was another non-technical piece that I enjoyed writing. I found myself sometimes hitting a
blocker with Ansible in which I didn't know how to do a certain thing and the documentation wasn't
very forthcoming with an answer. Usually after further tinkering or a few days trawling forums I
would be able to find the answer to my problem.

What spurred me to write about these solutions is that other Automation Engineers were running into
the same problems and I don't think that they should have to spend time trying to find the answer
if it is already found. So this was just a repository of documentation on common programming
patterns and problems that I encountered meant to augment the official documentation.

---

[Top](#top)
