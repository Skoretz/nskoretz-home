---
title: In Conclusion
author: Nicholas Skoretz
date: 2019-09-11
---

# In Conclusion

In conclusion, this was another great co-op experience. I had the benefit of not losing contact
with anyone at Symcor over the four months I spent at university in between official co-op terms as
I received an offer to stay on part-time.

Coming back to the office was a breeze as was onboarding. The work that I was doing is engaging
and really itches my problem solving side of the brain. A very fun work term overall. I would like
to recognize the other great co-op students that I got to spend time with, as well as
the Automation, Core Services, and RPA teams.

