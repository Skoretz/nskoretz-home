---
title: My Role
---

# My Role

## Core Services - Automation Team

I ended up in quite a fun team this term around. Core Services, once known as Reasearch and
Development (R&D) is full of extremely knowledgeable people and fun tools to use. The name of the
department changed to Core Services because other more and more sub-teams were coming under the
purview of R&D that weren't directly related to R&D, and this was making the purpose of the
department a little fuzzy.

Core Services now "owns" the Database Administrator (DBA) team, Tier I and II support teams and the
Telephony team, Robotic Process Automation (RPA) team as well as the team that I was on, the
Automation Team.

Symcor is using a suite of tools to automate processes at Symcor for many departments. If the task
involved automating legacy software or tools without APIs, that automation is handled by the RPA
team and their tools, UIPath and .NET. All other automation tasks fall to the Automation Team and
our tools, Ansible, Ansible Tower, and GitLab-CI.

## Automation Engineer

The role of Automation Engineers is to design, test, implement, and maintain automation pieces that
are requested from other departments in Symcor. If a department or team has a process that they
feel would be better off automated, they will contact the Automation Team Lead and add their task
to the automation funnel.

The funnel is constantly sorted and tasks are measured on their current cost in hours versus the
time saved by automating. The time is needed to design the automation and the complexity is also
taken into account to determine what makes the sprints.

The team works in an Agile way with daily stand-up meetings, sprints, and task and issue tracking
in JIRA. We work closely with the RPA team to integrate our automation pieces if the task contains
software that is legacy or can't be accessed by Ansible. We also work with the team that originally
requested the automation. We interview them, and understand the process that they follow so that we
can automate it correctly.

---

[Top](#top)
