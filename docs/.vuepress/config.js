module.exports = {
    title: 'nskoretz',
    description: 'Blog, Work Term Reports, and Everything Else',
    base: '/nskoretz-home/',
    dest: 'public',
    themeConfig: {
        nav: [
            { text: 'Home', link: '/' },
            /*{ text: 'Projects', link: '/projects/' },*/
            /*{ text: 'Resume', link: '/resume/' },*/
            { text: 'Work Term Reports',
                items: [
                    { text: 'Terms 1/2: Symcor', link: '/t12/' },
                    { text: 'Term 3: Symcor', link: '/t3/' },
                    { text: 'Term 4/5: CaseWare', link: '/t45/' }
                ]
            },
            { text: 'Colophon', link: '/colophon' }
        ],
        sidebar: {
            '/t12/': [
                '/', /* home?*/
                '', /* /t12/README.md which is 'About Symcor' */
                'departmentRole', /* /t12/departmentRole.md */
                'projects', /* /t12/projects.md */
                'goals', /* /t12/goals.md */
                'conc' /* /t12/conc.md */
            ],
            '/t3/': [
                '/',
                '',
                'role',
                'projects',
                'goals',
                'conc'
            ],
            '/t45/': [
                '/',
                '',
                'position',
                'projects',
                'goals',
                'conclusion'
            ]
        }
    }
}
