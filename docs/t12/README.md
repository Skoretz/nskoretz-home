# About Symcor

---

<img src="../assets/images/it12/logoCT.png" alt="The Symcor logo" style="width:20%;height:20%">

**By the Banks, For the Banks**

That is not the official slogan of Symcor by the way, but it could very well be.
It's a great summary of the history and purpose of the company.
And that slogan shouldn't be misinterpreted as something bad either, Symcor was created with the
express purpose of solving a problem, and they do it very well.
What might that problem be, you ask? The problem is cheque processing, and the answer ... is Symcor!


## History

Alright, alright, let's skip the theatrics and get right down to the history.
Symcor was founded in 1996 by BMO, TD Canada Trust, and Royal Bank to handle cheque processing
as aggregate.
Setting up and maintaining cheque processing at each bank was a bit of a major pain point for them.
At the time, cheques galore flowed between the banks and the money math became such that,
it was more efficient and cost effective to pool together the resources to create a single entity to
process all the cheques for the founding banks.
And hey, presto, Symcor was born.

## Symcor at Present

Although cheque processing remains one of Symcor's main three operating divisions, cheques as a
whole are declining, and have been for quite a while.
At some point Symcor moved into statement printing, which is one of the other major divisions.
Nearly any bill or statement you receive was probably printed by Symcor.
From Hydro One and Cogeco, to Saskatchewan Power and BMO, the statement is a Symcor product.
Now you may ask yourself, paper statements must be on the decline as well surely, pretty much each
company is begging me to switch to e-statements.
You would be correct in that assumption.
Symcor had that same thought (I assume), so they expanded into digital statements. Now the
statements you receive in your inbox are hosted on Symcor servers,
and may have been designed in part by Symcor.

<br />
<br />
<div style="display:flex;justify-content:center">
  <img src="../assets/images/it12/logo-hydroone.jpg" alt="BMO logo" style="width:100px;height:100px">
  <img src="../assets/images/it12/logo-cogeco.png" alt="BMO logo" style="width:100px;height:100px">
  <img src="../assets/images/it12/logo-saskpower.jpg" alt="BMO logo" style="width:100px;height:100px">
  <img src="../assets/images/it12/logo-bmo.jpg" alt="BMO logo" style="width:100px;height:100px">
</div>
<br />
<br />

As you may have deduced from the companies I name-dropped above, Symcor has expanded from just the
banks to a myriad of other Canadian companies. After an ill-timed expansion into the US right on the
eve of the 2008 financial meltdown, which folded and set the company back a little bit, Symcor is
expanding into the Insurance market and continuing to innovate in the banking sector.

## Symcor's Future

Being a central data mart for physical and digital documents being sent between the banks in Canada,
Symcor has a unique position that is can tap into. Cheque fraud is in the rise in Canada, especially
with the rise of cheque cashing on banking apps, it's quite easy to cash a cheque a multiple banks
very quickly. Currently in Canada, the first bank with which the cheque is cashed receives the money
from the issuing bank. Any others are left down money if they cash the fraudulent cheque. Symcor is
currently rolling out Duplicate Cheque Detection, a system to scan and compare cheques being
processed in real time. As well, Symcor is developing an Open Banking platform that will capitalize
on the insights that Symcor has collected and is collecting, packaged and redistributed to the
member banks.

<div style="display:flex;justify-content:center">
  <img src="../assets/images/it12/logo-coriq.png" alt="COR.IQ logo" style="width:40%;height:40%">
</div>


New departments are popping into existence all the time at Symcor as they go through a period of
rapid innovation. A bit of a forced innovation at that, seeing as one of the major arms of Symcor
has been on the decline for a while now. But this innovation, in step with a
"Culture Transformation" has provided me as a co-op student some interesting opportunities to touch
the company and leave an impact, however slight. And let me tell you, I really appreciated that.

<div style="display:flex;justify-content:center">
  <img src="../assets/images/it12/logo-innochal.png" alt="COR.IQ logo" style="width:30%;height:30%">
</div>

## Facts and Knowledge

To wrap up this section, I leave you with a few quick facts that blew my mind when I learned them.

* Even though physical cheques are on the decline, Symcor's Prince Andrew Centre location processes
one every 0.3 seconds.
* Symcor's Tech Avenue location has three printers that print 80'000 statements an hour.
* Symcor has 10 locations across Canada as well as an offshore team in India.
* Symcor is Canada Post's biggest client, due to the massive amount of mail that Symcor pushes on
behalf of Canadian companies.
* Symcor has 2.5 petabytes of encrypted bank data in archive.

---

[Top](#top)

