---
title: Goals and Reflection
---

# Goals and Reflection

As part of the co-op process at the University of Guelph, students are required to create goals.
These goals can cover soft or hard skills, but students are required to track progress and set
small milestones for the goals, which is a really great skill. Below are the goals that I set for
myself, how they evolved, and my progress towards them.


## Goal 1

**Develop my oral communication skills and my presentation skills while on the job.**

*Type* : COMMUNICATING - Oral Communication

<details>

<summary>Click for details</summary>

### Action Plan
- Play an active part in creating the presentation for my group's idea in the Symcor Innovation
Challenge.
- Develop presentations on the technologies that I am learning and present them.
- Attend SymTalks by other coworkers to get an idea with different presentation styles and to learn
from them.

### Measure of Success

- Deliver a great presentation to the judges.
- Deliver three technology presentations to my manager Vik.

### Progress

Progress on this goal by running demos. When I finish a coding task I demo it to my team and field
questions. Good practice with impromptu presentations.

### Reflection

All in all, I feel that I progressed well on this goal and the fact that I had identified this as
one of my five goals dictated some of my decisions. I had options to either present or be passed
over and I decided to present to see what would happen. The measures of success were a bit different
in the end as I mentioned in the progress section. I ended up doing more demos to various teams
rather than doing presentations to Vik, my manager. But I think that the team demos ended up being
better for me in the development of communication skills.

</details>


## Goal 2

**Increase my working knowledge of enterprise database systems and get a better grasp of the SQL
language, its syntax, and query structure.**

*Type*: LITERACY - Technological Literacy

<details>

<summary>Click for details</summary>

### Action Plan

- Learn by reading through existing SQL scripts that are used for work and documenting them.
- Creating my own SQL scripts to gather data.
- Learn by reading the SQL 9.2 Documentation.

### Measure of Success

- Document all of the .SQL files that were left to me, so that the next Data Developer Co-op knows
what is happening.

- Design a plan that describes how I could implement a SQL Database System at home for my personal
Bartender App project.

### Progress

Progress on this goal by organizing and refactoring pl/pgSQL functions.

### Reflection

So, I progressed along one of the measures of success on this goal, and overall I feel like I have a
great grasp of SQL-like languages after this co-op. The second measure of success has been shelved
for the moment. But in general, I believe that there is still a demand for databases, whether in
personal projects or in school projects. Having the working knowledge to whip up a database, tables
and schema is in my opinion, quite an asset.

</details>


## Goal 3

**To increase my knowledge and familiarity with website design, including languages and frameworks
such as HTML, CSS, node.js or PHP.**

*Type*: LITERACY - Technological Literacy

<details>

<summary>Click for details</summary>

### Action Plan

- Get a working knowledge of the Health Check Web App that is part of the Health Check project I am
working on.
- Work towards rebuilding / reconfiguring the Health Check Web App.
- Work towards building my own website / technical blog for the Co-op end of term report.

### Measure of Success

- Rebuild / Reconfigure the Health Check Web App. At minimum make meaningful changes that improve
the usability of the Website.
- Build my blog.

### Progress

Progress on this goal has slowed down. But I still believe that I've received lots of knowledge and
experience.

### Reflection

As the progress statement says above, I received a working knowledge of the Health Check Web App,
although I did not rebuild or reconfigure the web app. I am on a team to find an alternative
solution to our whole current APM methodology, so the Health Check Web App is going with it.

As you are no doubt reading this on my personal website / blog, progress on this measure of success
has gone well. My experience with Vue.js in building a demo of an internal recognition website for
the Symcor Innovation Challenge motivated me to use Vue and VuePress for this website right now.

</details>


## Goal 4

**Increase my written communication skills and improve my habits by creating meaningful comments and
documentation for all code that I work on.**

*Type*: COMMUNICATING - Written Communication

<details>

<summary>Click for details</summary>

### Action Plan

- Create a style guide for the .SQL Health Check files.
- Develop a process or learn and adopt an existing process (javadocs, Doxygen) to create standard,
meaningful, and informative comments and supporting documents.

### Measure of Success

- Implement the .SQL style guide

### Progress

Progress on this goal is continuing. I write documentation and overview documents for the Ansible
tasks that I am assigned, as the team recently moved to a different project code.
As well, I am writing a post-mortem for the Innovation Challenge project, as it as well is moving to
another team.

### Reflection

This goal I believe was achieved. I completed the task that was outlined as a measure of success. As
well, I was creating many Ansible playbooks comprised of many roles. Each role had a document
regarding how to use it and other details such as default values, and the purpose for creation. I
believe that these role readmes acted as good documentation practice for this goal.

</details>


## Goal 5

**Develop DevOps skills and learn automation tools that will help me in the future.**

*Type*: LITERACY - Information Literacy

<details>

<summary>Click for details</summary>

### Action Plan

Automate processes involved with Health Check, such as the data input and the graphing. Perhaps
using R and ggplot2.

### Measure of Success

Aim to have 2019 Health Check complete by December or early January.

### Progress

Progress on this goal has advanced significantly, with over 50% of my work being automation tasks as
of now. I am learning a lot about Ansible that I intend to use it on some home projects.

### Reflection

I have definitely achieved this goal, though not in the way outlined in the measures of success and
action plan. The Health Check is coming under consideration as part of the shift in our thinking of
APM in Enterprise Architecture, so sadly, progress on that dimension has stopped. Although, I was
leant out to the Automation team for various projects and I gathered a lot of experience with
Ansible, Ansible Tower / AWX, and GitLab-CI. These tools fit squarely into the automation category,
and I am even automating the publishing of this website using GitLab-CI.

</details>

---

[Top](#top)

