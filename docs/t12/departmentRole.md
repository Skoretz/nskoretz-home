---
title: My Role
---

# My Role

## Enterprise Architecture

I had never heard of this department before I was hired at Symcor.
I also haven't been fully briefed on everything the department does, but this is what I understand.
At Symcor, the Enterprise Architecture (EA for short) department collects and codifies existing
standards from all the departments in IT.
If standards need to be developed, the appropriate people are engaged to create the standards,
though these would be subject matter experts, rather than EA itself.

As well, EA at Symcor has the role of disseminating information to employees in IT.
Whether that be news, information about standards changes, or otherwise.
This is done through the TechHub project, which releases monthly newsletters and maintains a few
internal sites and news feeds to keep people informed.

The last major thing that EA handles at Symcor is Application Portfolio Management (APM).
APM is the collection and storage of real time or other measured metrics about the pieces of
technology being used in the company.
This project kind of spans the divide between the architecture and the application sides of IT.
The APM collects information such as, uptime, number of high, medium, and low severity incidences,
the IT strategy of the specific tool (whether to maintain, invest, or sunset),
as well as the state of documentation and support for the tool.
All of this information is being gathered for insights on Symcor's technology landscape.

## Data Developer

The Data Developer's main project is the APM. Symcor's current APM had a Java web front-end and a
PostgreSQL back-end.
My role as Data Developer was to maintain the APM's database.
I update, delete, modify information on request.
I also provide reports if somebody needs specific information that they cannot find on the website
or in the published APM books.

There were quite a lot of pre-written queries and pl/pgSQL functions due to the fact that the APM
has a yearly intake period,
where data from different sources comes in to the database.
At this point in time, the process is mostly manual, slightly automated with the help of these
scripts, but no pipeline exists for the data intake.

I also did other EA duties, such as market research on Enterprise Architecture Management Systems
(EAMS) and different APM offering that already exist in the market.
The 2018 Innovation Challenge fell under the purview of EA this year,
so I was on the organizational committee for that and I also was part of the team that won 2nd place
for our idea.

In essence, that's the Data Developer role in EA in Symcor.
I have been able to pick up odd jobs and projects from other departments and teams, such as from the
Automation team, which has kept me quite busy.

---

[Top](#top)

