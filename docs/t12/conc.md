---
title: In Conclusion
author: Nicholas Skoretz
date: 2019-01-10
---

# In Conclusion

In conclusion I have to say that this was a great first co-op experience. There were quite a few
unknowns going into the job. Such things as, never even hearing about Symcor before applying, nor
knowing that the Enterprise Architecture (EA) department was a thing. But, here we are on the other
side after a very fulfilling co-op job.

I really enjoyed working with other co-op students on various projects, and I also enjoyed having
the flexiability to work with other teams on technologies that I am interested in. Working in EA has
really given me a different perspective on infrastructure and applications in IT. Working with
automation tools and modern web development tools was a great opportunity, not to mention getting a
great grasp of enterprise database management.

I would like to thank Vik Patel and Tori Throckmorton of the Enterprise Architecture department for
giving me the opportunity of having a great co-op term. I'd also like to thank the summer and fall
cohort of co-ops for making ordinary days fun. Thanks Mike MacDonald and Chris Wojdak for a great
interview. And finally thanks to Ivan Welsh, Saba Shariff, and Anthony Koso for letting me join
teams and projects in your department.
