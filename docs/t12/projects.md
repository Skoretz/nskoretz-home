---
title: Projects
author: Nicholas Skoretz
date: 2018-12-21
sidebarDepth: 3
---

# Projects

[p-role]: ./departmentRole.md
[t-postgres]: ./technologiesTools.md

I was fortunate enough to be involved in a lot of projects over my eight month co-op. From technical
projects like Ansible automation and prototyping a website with the Vue.js framework to
non-technical projects, such as being on the committee that ran the Symcor Innovation Challenge, a
hackathon-eque creative competition.


## Terms
- APM = Application Portfolio Management
- APMWA = Application Portfolio Management Web Application
- RSP = Robert Speck Parkway
- SDX = Symcor Data Exchange


## Technical Projects

### Application Portfolio Management Database Maintenance

  <figure>
    <img src="../assets/images/it12/proj_hcdb.png"
      alt="A screenshot of a network graph representation of the table connections."
    >
    <figcaption>
      A network graph representation of some of the connections between tables in the APM database.
    </figcaption>
  </figure>

As you may have read in the [My Role][p-role] page, the APM is a database that collects
information such as, uptime, number of high, medium, and low severity incidences, the
IT strategy of the specific tool (whether to maintain, invest, or sunset),
as well as the state of documentation and support for the tool. Some of the maintenance tasks
included going into the tables and updating records such as application owner and the like.

Other times, people from other departments from within Symcor needed a "pull" of the certain
information from the database that wasn't being served up on the Web Application. In those cases
I would write a query that would return their requested information. I then distributed it to them
in ".csv" format.

Most PostgreSQL installations come with the pl/pgSQL language, which stands for
"procedural language / PostgreSQL". This language allows for functions and variables that can
"wrap" a query, this making it easier to script. The APM has many of these functions saved in .sql
files, although most were out of date and uncommented. I went through and refactored most of these
pl/pgSQL functions.

*Technologies:*
- PostgreSQL
- pl/pgSQL

---


### Application Portfolio Management Web Application

The APM has a web app front end denoted as the APMWA. This allowed for a non-technical interface for
other employees in Symcor to see some data in the APM for applications. The web app also allows for
attestation of the information for a particular application by the application owner, as well as
report generation into HTML, .docx, or .pdf. Aside from just generally familiarizing myself with
this interface for the APM, I had to add the ability for the reports to be generated as .docx, this
was so that compilation into the [published books](#apm-publishing) was easier.

*Technologies:*
- Apache Maven
- Apache Tomcat
- Java

---


### Innovation Challenge Submission / T.A.G. Initiative

  <br />
  <img src="../assets/images/it12/proj_tag.png" alt="The TAG initiative logo." style="width:50%;height:50%" >

At Symcor, a yearly challenge is run where individuals submit innovative ideas and form a team
around them. The teams have 24 hours to create a demo or presentation of their idea and present
it to a panel of judges for prize money, and the change for the teams idea to be implemented.

I formed a team with other co-op students that proposed a solution to the lack of individual
recognition in the company. One of the driving reasons for this was the terrible interface of
the existing system, which was PeopleSoft.

As part of our solution, we designed a website that would act as the central hub for the new
initiative, named T.A.G. which stands for Thank, Acknowledge, and Give. Employees could nominate
others for recognition, track nominations and points, and purchase Symcor branded "swag" as prizes.

The website was written with Vue.js to be a single-page app. Various components were created, such
as a network graph that tracked trophies as they were passed around from employee to employee that
took data from our Postgres database and updated live, as well as some live statistics.

Integration with email was planned, as well as a potential mobile application, but as this was just
a working demo
these components weren't completed. Out team won second place and a prize pool of \$1,000. Work did
continue after the challenge, although sadly the company went with another solution for the problem
that was identified. Regardless, familiarizing myself with the Vue framework was a lot of fun, and
the experience influenced the decision to create this website with VuePress, a static-site generator
that runs on Vue.

*Technologies:*
- D3.js
- JavaScript / Node.js
- PostgreSQL
- CSS / Sass
- Vue.js Framework

---


### Symcor Data Exchange Projects

The Symcor Data Exchange (SDX) is a system of programs and protocols that lets
client institutions send imaged cheques, statements, and other files to Symcor and to other clients
securely. I worked to automate various aspects of SDX as detailed below.

#### SDX Client Onboarding Automation

Due to the many customization options SDX has to accommodate clients, when a new client is onboarded
to the application a client profile needs to be filled out for them, along with some other
configuration files and a directory endpoint. Currently, this process is very manual, requiring
knowledge of the company and how they like to structure their directories for example. A
Transmission Template is also created for each new client, it outlines the files that will be
transmitted, the Symcor liaison, contact information from someone from the company, etc.

I created an Ansible playbook that would read a portion of the transmission template, parse the file
into Ansible variables and then create the required directory structure and fill in the config files
from templates using Jinja2. Now instead of needing to remote in to various machines and create
directories and files all over the ecosystem, the person in charge of onboarding would only have to
work with one file, which would then create the rest of the config files.

#### SDX file resend automation

Some of the files that are transferred by clients are quite huge. On their way out of the SDX system
they can take about an hour to transfer. If a network connection drops during the transfer,
currently the affected file is dropped into an "archive" directory and once the transfer failure is
detected it needs to be manually re-staged for the transfer to re-initiate.

I was tasked with creating an Ansible playbook that queries the Service Now API for Incident Records
(IRs) related to file transfers failing. The playbook then parses the information, re-stages files,
and checks for success messages in the associated log files. The automation is designed to live in
Ansible Tower and is scheduled to run at intervals. It can also be ran manually. Currently it was
designed to operate on a small portion of the files being transferred into one zone, but after more

#### SDX server scaffolding automation

Due to increased load of files being transferred across SDX, more servers need to be stood up.
I am working on a team to immediately create two new SDX servers, as well as to automate the
process so that future deployments can be automated.

I have worked on various small automation tasks for this team. I am currently writing the
automatic deployment and configuration for a file transfer program that SDX uses. This
automation piece is being done with a mix of GitLab-CI and Ansible. Another task concerns
pruning data older than 45 days from the servers without clobbering important configuration files
or other critical data. White this could be done with a cron job, I have completed this task with
Ansible. The thought process being, that if it lives in Ansible tower it can be scheduled to run on
all servers in an inventory without having to set up individual cron jobs on each server.

*Technologies:*
- Ansible
- Ansible Tower / AWX
- Git
- GitLab-CI
- Jinja2


## Non-Technical Projects

### APM Publishing

<img src="../assets/images/it12/proj_hcbook.png" alt="The cover of the 2018 Application Health Check." style="width:50%;height:50%">
<br>
<br>


Another major function of the APM project is to create a report each for applications and
infrastructure that is in the APM. These are published yearly on the Symcor intranet as well
as in print form, coming out to a 400 page tome for each category. This took a lot of time, as
the reports needed to be generated and then formatted into a book.


---


### RSP Innovation Challenge Committee

<br>
<img src="../assets/images/it12/proj_ic.png" alt="The Innovation Challenge 2018 logo." style="width:50%;height:50%">
<br>
<br>


Along with being in a team that participated in the Innovation Challenge, I was also on the
organizational committee for the Challenge at my location, Robert Speck Parkway (RSP). The
committee's tasks included booking the working and presentation spaces, confirming the judges,
printing posters, participant T-shirts, and other marketing material, not to mention fielding
the idea submissions and running team forming meetings to collect participants who wanted to work
on idea together.

This experience was a great learning opportunity. We had a \$10'000 budget that we
had to manage, between the finalist prizes, door prizes, food, and all the other costs associated
with running an event of this scale. In total we had 10 teams averaging 4 participants per team.
We had a panel of three judges, various
subject matter experts that were helping the teams during the build and design stage, and an
audience of around 100 for the final presentations.


---


### Operations Innovation Challenge Committee

<br>
<img src="../assets/images/it12/proj_ico.jpg" alt="The Innovation Challenge banner." style="width:50%;height:50%">
<br>
<br>

Due to the success of the Innovation Challenge at RSP for fours years in a row, the Innovation
Challenge was going Canada wide and expanding to all of our operational sites. The organizational
committee at RSP was tasked with providing support, marketing material, and a general budget and
plan for each of the individual sites to follow. Bi-weekly meetings were set up with General
Managers from each site to communicate issues with the roll-out.

Myself and a few other co-ops travelled to the nearby Mississauga and Toronto sites to run sign up
booths, idea generation meetings, and design thinking meetings for participants at the other sites.
I was also able to see the final presentations from the other Mississauga location and provide
feedback on their presentations ahead of the final, judged presentation.

This experience was very fulfilling, being part of a country-wide team and communicating with
individuals way above me on the corporate ladder was a great opportunity.


---


### United Way Campaign

<br>
<img src="../assets/images/it12/proj_uw.jpg" alt="The United Way banner." style="width:50%;height:50%">
<br>
<br>

As a bit of a get to know you exercise for the new cohort of co-ops that started in September, we
were tasked with running the United Way yearly campaign. The team had a budget of \$1'000 to do
whatever we wanted with, but the goal was to raise as much as possible. I helped create meeting
minutes, Gantt charts, and whatnot to help us organize our activities. All in all, we ran a
50 / 50 draw, prize raffles, bake sales, and candy-grams.


---


### Innovation Challenge Post-Mortem and Handover

Recently at Symcor a new innovation department was started. The Innovation Challenge fits more
into that department rather than Enterprise Architecture, so I was tasked with organizing all of
the collateral that we generated in running this years challenge and the challenges from the
operations sites. I also had to create "roles" such as treasurer, technology liaison and others to
streamline the activities. I also created charts and timelines and organizes it into a package to
hand over to the new department.


---


### APM Solution Discovery


After identifying the EA department identified that it would like to revamp the Health Check and
purchase an Application Portfolio Management software, a team was put together to apply design
thinking to concretely identify the problems and pain points that we have, and how to solve them in
the best way for Symcor. I am part of this team that is comprised of Enterprise Architecture folks
and members of the Digital Delivery and Solutions department.

This is still an ongoing project. We are in the process of filling out a discovery canvas, as well
as interviewing stakeholders who use the current Health Check to see what they're needs are for the
future APM product or solution. Symcor has hired me part-time during my winter university semester,
and this project will be the my main focus.

---

[Top](#top)
