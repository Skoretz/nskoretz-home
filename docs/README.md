---
home: true
---

::: tip Check it Out!
Read about my latest [work term experience as a DevOps Developer here!](./t45/README.md)
:::

My name is Nicholas Skoretz. I am a fifth-year Software Engineering (co-op) student that has just
finished an eight month work term at CaseWare. I enjoy rock climbing, worldbuilding, cooking and
bartending for friends.

Please see my [GitHub][l-gh], [GitLab][l-gl], and [LinkedIn][l-li] for more software and work
related things.

For more about me, see my old [travel blog][l-rc].

Feel free to contact me at [nskoretz@uoguelph.ca](mailto:nskoretz@uoguelph.ca) or
[nskoretz@pm.me](mailto:nskoretz@pm.me).

[l-gh]: https://github.com/nskoretz
[l-gl]: https://gitlab.com/Skoretz
[l-li]: https://www.linkedin.com/in/nicholas-skoretz-444905169/
[l-rc]: http://remotecorner.blogspot.com/

---

![](./assets/images/me_small_frame.jpg)

Cheers,
Nick

