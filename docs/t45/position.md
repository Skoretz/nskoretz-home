---
title: My Role
---

# My Role

## DevOps Developer

While I was employed at CaseWare, I was a DevOps developer. This role encompassed many activities
and the role actively changed when I switched from the DevOps team to the sister-team, InfraOps,
midway though my eight-month term. We'll take a look at the different types of activities that
I did in the following sections focused on the teams. Though in general, I worked with the cloud
infrastructure and pipelines that underpinned our cloud services, maintaining and adding
functionality as new products are developed and come online.

## Cloud Operations - DevOps

I started at CaseWare on the DevOps team. The Cloud Operations department lies at the top, with the
DevOps and InfraOps teams underneath. In terms of general responsibilities, the DevOps team is in
charge of creating and maintaining the CI/CD (Continuous Improvement/Continuous Delivery)
pipelines that we have for our offerings. A lot of the application-level responsibilities are
handled by the DevOps team. They also have to respond to code-related performance degradations.

My responsibilities on the team included creating microservice deployment pipelines with Atlassian
Bamboo and AWS CloudFormation, testing other pipelines in Jenkins and automating manual steps in
deployment procedures. More details on specific projects are on the [Projects](projects.md) page.

## Cloud Operations - InfraOps

The sister-team to the DevOps team is InfraOps. InfraOps is responsible for the cloud
infrastructure that underpins our products and services. This involves managing our EC2 resources,
network resources, and any pipelines that provisioned new infrastructure. InfraOps also manages
our logging and monitoring capabilities and we have to respond to service degradations involving
infrastructure.

My responsibilities on the team mainly involved a large project to roll-out New Relic
Infrastructure Monitoring to our infrastructure resources. I was initially part of a team working
on this, but due to other circumstances, I got the chance to lead the project and become the point
of contact for New Relic at CaseWare. You can read more about this project on the next page.

---

[Top](#top)
