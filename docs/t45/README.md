# About CaseWare

For my fourth and fifth co-op term, I had the opportunity to work for CaseWare in Toronto.

<img src="../assets/images/it45/logo_cw.png" alt="The CaseWare logo" style="width:20%;height:20%">

CaseWare is a software company that creates and sells solutions for accounting, auditing, tax, and
various other financial needs to three key markets. CaseWare develops solutions for accounting
firms, for corporate, and for the government. Good portions of these three solutions are built upon
the cloud platform that CaseWare has developed.

What helped me understand what CaseWare offers was a comparison to the tech company Shopify.
Rather than offering a strict financial product like say, FreshBooks, CaseWare has developed a
platform for financial software like Shopify. CaseWare develops software on/for their platform,
but CaseWare also has a global distributor network that develops applications on the CaseWare Cloud
platform.

## History

CaseWare was founded in 1988, and released their flagship product a year later, Working Papers.
Working Papers launched as a desktop application, but over the years has evolved into a hybrid
cloud offering. Working Papers became a collaborative editor for "engagements", a Google Docs for
accounting documents, though years ahead of Google. A suite of products has been built over the
years catering to the three key markets above and have put CaseWare in the position of a market
leader in a lot of countries.

---

[Top](#top)
