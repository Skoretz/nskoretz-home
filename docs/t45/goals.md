---
title: Goals and Reflection
---

As part of the co-op process at the University of Guelph, students are required to create goals.
These goals can cover soft or hard skills, but students are required to track progress and set
small milestones for the goals, which is a great skill. Below are the goals that I set for
myself, how they evolved, and my progress toward them.

## Goal 1

**Become familiar with AWS and some of its breadth of services.**

*Type* : LITERACY - Technological Literacy

<details>

### Action Plan

- Leverage the knowledge of my team members to understand how CaseWare uses AWS in a corporate
  setting
- Work on various projects and tickets that involve AWS components

### Measure of Success

- Explore our AWS EC2 instances and other supported services with New Relic
- At work, contribute to the AWS knowledge base of documentation at CaseWare

### Reflection

I can confidently say that I developed a good grasp of the core AWS services and how
they're used at CaseWare. Initially coming into a toolbox of over 200 tools is
intimidating, but I can now navigate EC2, AMI, SecretsManager, S3, and ECS among others.

</details>

## Goal 1

**Gain a deep knowledge of New Relic and its agents.**

*Type* : LITERACY - Technological Literacy

<details>

### Action Plan

- Learn how the agents are deployed and what they're used for
- Implement alerting, dashboarding and other services offered by New Relic to make the most of our
  infrastructure data

### Measure of Success

- Implement the New Relic Infrastructure agent in the majority of our EC2 instances in the test
  environment
- Implement helpful alerting that will be used by the team to keep track of incidents

### Reflection

This was achieved. I ended up leading the New Relic roll-out for our infrastructure and became the
SME of how we set up New Relic at CaseWare.

</details>


## Goal 3

**Learn more about containerization concepts and how they can apply to development.**

*Type* : LITERACY - Information Literacy

<details>

### Action Plan

- CaseWare has quite a few products in containers and is exploring cloud container management
  solutions
- Work on or around projects involving EKS (Amazon's Elastic Kubernetes Service) or the existing
  container system

### Measure of Success

- Apply containerization concepts that I learn to my personal and co-op website
- Use my gained knowledge to deploy microservices to the New Relic Docker Swarm or EKS cluster

### Reflection

I was able to use my knowledge of containerization to deploy three microservices at CaseWare and to
generally interact, add, and remove stuff from the microservice pipelines.

I have also learned enough about containerization to know that it wouldn't benefit my
website architecture, as I can use something like a static site to better effect.

</details>

## Goal 4

**Develop my problem solving and analysis skills.**

*Type* : CRITICAL & CREATIVE THINKING - Problem Solving

<details>

### Action Plan

- Within the department, various members rotate on-call schedules to deal with unexpected DevOps
  problems from internal and external clients
- Assist or join the 9-5 on-call rotation

### Measure of Success

- Assist with at least 5 on-call tickets, whether on-call or not. Contribute knowledge or help

### Reflection

While I got to participate in 5-10 on-call tickets, I did not get the amount of on-call experience
that I wanted. I did get problem-solving and analysis skills on the New Relic project though, which
was good.

</details>

## Goal 5

**Demonstrate the capability to work well with team-members, non-team co-workers and external
vendor.**

*Type* : PROFESSIONAL & ETHICAL BEHAVIOUR - Teamwork 

<details>

### Action Plan

- Work with various non-team stakeholders in the company about their requirements and needs
- Work with external vendor team to solve problems and bring up product improvements
- Work with team members to effectively complete tasks that work toward the requirements


### Measure of Success

- Roll-out the implementation of New Relic at CaseWare. Primarily focused on the Infrastructure
  monitoring side, but perhaps helping with Application Performance Monitoring (APM) as well

### Reflection

Since I ended up leading the New Relic roll-out at CaseWare, I interfaced with internal development
teams, and the New Relic liaison team to communicate CaseWare's needs and requirements and to
coordinate work. I feel like I successfully developed my inter-team communication.

</details>


---

[Top](#top)
