---
title: In Conclusion
---

Overall my time at CaseWare was a blast. The work was engaging, and both teams that I worked with
were extremely friendly and hopeful. The major thing that stood out to me was the fact that I was
treated as another team member. I was not given a "co-op project" that didn't matter, I was given
normal team tests and I was expected to do them.

For me, this pushed me to learn more and work harder, which seriously developed my skill in the
process. I can come away from this co-op feeling like I grew a lot, and feeling confident in myself
for the future.

---

[Top](#top)
