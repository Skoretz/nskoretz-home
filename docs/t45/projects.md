---
title: Projects
---

# Projects

## DevOps Projects

### Collaborate Deployment Automation Pipeline (CDAP) Testing

One of the integral pieces of our cloud platform is called Collaborate. An initiative to automate
the deployment of Collaborate for testing and releases was started, abbreviated CDAP. This pipeline
ran primarily in Jenkins, though also runs various Bash, Python, and Ansible scripts as steps in
the build. It had various cases that it needed to handle, as it could release new versions of
Collaborate on new servers, or it could migrate data and "firms" from older server versions to
newer ones.

Small sidebar on firms, CaseWare uses the word "firm" to denote a client and their associated data
and accounts.

I was tasked with testing various use cases on the pipeline and observing the output or any errors
that arose. I reported errors and some fixes to my co-worker who was leading the project.

### Creating Pipelines for New Microservices

CaseWare's microservices are Python, Java, or Node.js programs that are deployed to Docker swarms
or Amazon EKS. Pipelines that deploy the microservices per Git branch are convenient for testing,
and With new microservices being created all of the time by various development teams, these
pipelines save a lot of time as opposed to manual deployment.

During my time on the DevOps team, I created pipelines for three microservices. This involved
integrating Atlassian Bamboo jobs with CloudFormation and EKS so that the services can be deployed
correctly. 

### Operational Excellence Production Checklist

Since the DevOps team handles all deployments and most of the service disruption
response, we wanted to have a standard that development teams could reference before their software
would have a pipeline built for it. No formalized document had existed yet and I was tasked with
researching and coming up with a Production Checklist that can be reference by new projects before
they get to deployment. While some of the findings were based on supplemental research that I did,
a lot of the checklist just involved formalizing processes and knowledge that we already had into
one convenient document.

This checklist was immediately put into use while I was at CaseWare, with various new microservices
referencing the checklist and the best practices within.

### Standard Operating Procedure (SOP) Rewrite - How to Delete a Firm

The DevOps team had an outdated, manual process for deleting a "firm" and associated data. I was
tasked with rewriting the SOP to reflect current processes. I also had to write the SOP in a highly
detailed way, as eventually, the plan would be to automate this process. Firm deletion is used a lot
in testing, with adding and removing test firms, as well as when a client is renamed or wants to
end their relationship with CaseWare. This process involves multiple products that are owned by multiple
teams within the company.

I had to collect procedure information from four distinct teams, verify it and formalize it into an
up-to-date, detailed SOP on the internal Confluence.

### Various 9-5 On-Call Tasks

One of my goals for this semester was to develop my problem solving and quick thinking skills this
semester by participating in the 9-5 on-call rotation. I didn't get the chance to join the rotation
on this team, but I did throw my hat into the ring and I picked up around five to ten tasks that
showed up in the on-call channel. These tasks involved fixing incorrectly configured pipelines and
identifying deployment failures among other DevOps tasks.

## InfraOps Projects

### Elastic APM

I switched teams to the InfraOps team around mid-April, right around the time they were deciding on
which solution we would use for Application Performance Management (APM). APM involves an "agent"
inside of your application that is monitoring metrics and sending them to a central location for
analysis. This is different from logging, which specifically looks at output text lines of
a program. The first iteration of the APM initiative settled on Elastic.co's APM solution, Elastic
APM.

Myself and my coworker Phil were assigned to design a plan to roll-out the agents to our services
and to set up the central monitoring interface (Kibana). Around two weeks into our deployment,
negative interactions between the agent and one of our products were observed affecting performance
during testing. This was not desirable and it prompted a pivot away from Elastic APM to
New Relic, a different solution.

### New Relic

After the pivot away from Elastic APM, the new service chosen for APM was New Relic. New Relic has
many other services that it offers that support APM, and myself and Phil we're tasked with
rolling-out APM, Infrastructure monitoring, Alerting, and graphing. The system not only had to
support internal CaseWare users but also our distributors. We had to design an account system to
ensure the separation of data between distributors. I also wrote documentation for the management of
New Relic that the InfraOps team will have to manage, as well as documentation for the
Infrastructure agent deployment itself.

The APM implementation work was delegated to the DevOps team, though I helped with some of the
design and pipeline integrations.

#### New Relic Application Performance Monitoring (APM) Agent Roll-Out

I started the overall New Relic initiative on the InfraOps team, and it is "owned" by InfraOps.
However, the APM side of New Relic is very application-focused, and it falls under DevOps. I worked
with two DevOps team members to design a solution that will include the APM agent in the deployment
process for four of our major product lines. This involved a lot of pipeline modification in
Atlassian Bamboo and in GitHub Actions.

#### New Relic Infrastructure Agent Roll-Out

An infrastructure agent is an agent that is installed on the underlying OS, rather than inside of
the application, like the APM agent. These agents collect system and network metrics and communicate
them back to New Relic so that this data can augment the APM data. The InfraOps can also set
thresholds for certain metrics, and then alert on them. Furthermore, New Relic has some
integrations with popular software that allows custom metrics about those applications to be sent
to New Relic through the infrastructure agent. For example, you can monitor MongoDB or HAProxy in
more detail if you have an infrastructure agent and their respective integrations on the server.

The infrastructure agent roll-out was my main project for the last three months of my term. It
involved designing Ansible playbooks for agent installation in Windows and Linux, modifying AMIs
with Packer to include the infrastructure agent, and then testing and installing the agents on over
400 servers in the development and client acceptance environment.

As well, I created some basic alerting for common thresholds that the InfraOps team would like to
monitor. I also installed three integrations for external software that we would like to monitor
through the infrastructure agent, depending on if the server supported it.


---

[Top](#top)
