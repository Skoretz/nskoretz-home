---
title: Colophon
---

# Colophon


<div style="margin:auto">
<img src="./assets/images/logo_vuepress_100.png" alt="The VuePress Hero" style="display:block;margin-left:auto;margin-right:auto">
</div>

This website is built with VuePress, a static site generator that renders pages made up of CommonMark
Markdown and Vue components.


<div style="margin:auto">
<img src="./assets/images/logo_vue_100.png" alt="The Vue logo" style="display:block;margin-left:auto;margin-right:auto">
</div>


Vue.js is a JavaScript framework that allows you to make components. These components
can be in a single file consisting of an HTML template section, a JavaScript / Node.js
section, and a CSS (or your choice of Sass or Stylus) section. These single file components
can then be rendered into the Markdown files that make up the content of this site.

<div style="margin:auto">
<img src="./assets/images/logo_gitlab_100.png" alt="The GitLab logo" style="display:block;margin-left:auto;margin-right:auto">
</div>

The website's source code lives in [GitLab](https://gitlab.com/Skoretz/nskoretz-home/). GitLab-CI is
used to automatically build the /dist from source, and refreshes the site you're reading right now
when new changes are pushed.

