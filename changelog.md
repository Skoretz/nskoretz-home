# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Added
- /docs/t12/conc.md
  - Added a conclusion page!
- /README.md
  - Added TIP that leads people to my work term report.

### Removed
- General
  - "WIP" warnings from the pages of the Symcor work term report.


## 0.0.4 - 2019-01-09

### Added
- /docs/t12/departmentRole.md
  - Added title.
- /docs/t12/goals.md
  - Added all goal information from Experience Guelph plus my reflection on the goals.

### Changed
- General
  - Spell checking to all documents
- /docs/t12/products.md
  - Removed mention of page linking.
  - Minor content corrections

### Removed
- Markdown files for deprecated pages.


## [0.0.3] - 2019-01-01

### Added
- /projects.md; Will be moved to work term 1/2 once complete.
- /colophon.md

### Removed
- General
  - Personal Projects link from the nav bar
  - Resume link from the nav bar

### Changed
- /README.md (homepage)
  - Added links to other sites and services
  - Updated intro
  - Resized picture of me




## [0.0.2] - 2018-12-27

### Added
- General
  - Term 1 and 2 content pages to add sub-headers.
- /.gitlab-ci.yml
- /docs/assets/images/it12/
  - Added pictures for documents in the /docs/t12/ repository.
- /docs/t12/departmentRole.md
  - Added warning at top of page about WIP
- /docs/t12/README.md
  - Added links to pictures.
  - Added warning at top of page about WIP
- /package.json
  - Added bugs, homepage, author, repository information.
- /projects.md
  - Finished adding technical content.
- /readme.md
  - Added link to live site, filled in all sections.

### Removed
- /docs/.vuepress/dist/
 - And all subfiles. This was old artifacts from a previous local build. 

### Changed
- General
  - Removed nextlink, prevlink text from the bottom of pages.
- /readme.md
  - Fixed localhost link in "How to Run / Deploy" section.
- /package.json
  - Changed package name
  - Changed package version to 0.0.2
  - Changed description


## [0.0.1] - 2018-12-27

<details>

### Added
- General
  - Scaffolded the project. Initialized a node project and packaged VuePress as a dependency.
  - Added some basic homepage content.
  - Customized the navbar a small amount.
- /readme.md

</details>


[0.0.3]: https://gitlab.com/Skoretz/nskoretz-home/compare/78ec6e4a...b285aab
[0.0.2]: https://gitlab.com/Skoretz/nskoretz-home/compare/6310e507...78ec6e4a
[0.0.1]: https://gitlab.com/Skoretz/nskoretz-home/commit/6310e5071098a3ddb79c9e4bdeee6390555f42fa?view=inline
