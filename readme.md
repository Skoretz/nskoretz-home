[l-cl]: https://keepachangelog.com/en/1.0.0/
[l-dev]: http://localhost:8080/nskoretz-home/
[l-glp]: https://skoretz.gitlab.io/nskoretz-home/


# nskoretz-home

**See the site live on [GitLab Pages][l-glp]!**

This is a static site built with VuePress and deployed to GitLab Pages with GitLab-CI.
It will be used to house my co-op work term reports as well as act as a personal homepage for
information about my projects, resume, and interests. I might throw some blog posts up there too.

---

*Last Modified: 2019-01-03 (Nicholas Skoretz)*


## Contents
- [Brainstorm](#brainstorm)
- [Repo Breakdown](#repo-breakdown)
- [How to Run / Deploy](#how-to-run-deploy)
- [Footnotes](#footnotes)


## Brainstorm

<details>

### Minimum Viable Product (MVP)

The site should at minimum contain:

- Well formatted homepage
- Work term reports
- Colophon explaining how the site was built
- Resume

### Possible Extensions

- Personal Projects page
    - Word spinner
    - Trip Mapper
- Travel section for old blog posts and travel photos
- Contact and Links page
- Get a wrapper Vue object for Fontawesome logos implemented

</details>


## Repo Breakdown

- [/docs/](#docs)
    - .vuepress/
    - assets/
- [/.gitignore](#gitignore)
- [/.gitlab-ci.yml](#gitlab-ciyml)
- [/changelog.md](#changelogmd)
- [/package-lock.json](#package-lockjson)
- [/package.json](#packagejson)

### /docs/

The /docs/ folder contains all of the actual website content. Markdown files layout pages
and assets that are linked are also stored in this folder.

- /.vuepress/
    - The /.vuepress/ folder holds some configuration files for the laying out of the website.
        Such as, what is included in the nav bar and the side bar.
- /assets/
    - The /assets/ folder holds images and videos that are loaded in the Markdown files.

Aside from the contents in the above folders, other folders and files represent pages
of the website.

### /.gitignore

This file tells git to exclude certain file and folders whose names follow a pattern listed
in the .gitignore file. In this case, I'm telling Git not to track my local node_modules/
folder, as all of the packages are listed in the /package.json file and can be redownloaded
easily with npm.

### /.gitlab-ci.yml

The existence of this file in the repo means that this repo can use GitLab-CI and have
Runners perform actions. This file contains instructions for the Runners, when to activate
and what to do. In this case, I tell the runner to install my npm module dependencies, build
the static site and deploy to [GitLab Pages][l-glp].

### /changelog.md

This file tracks changes to the project in an easier to digest way over commit diffs. The format
is based on [Keep a Changelog][l-cl].

### /package-lock.json

`package-lock.json` is automatically generated for any operations where npm modifies either the
node_modules tree, or `package.json`. It describes the exact tree that was generated, such that
subsequent installs are able to generate identical trees, regardless of intermediate
dependency updates.[^1]

[^1]: See the [package-lock.json documentation](https://docs.npmjs.com/files/package-lock.json).

### /package.json

This file is a configuration file for npm that specifies publishing information and some
general layout of your package. [^2]

[^2]: See the [package.json documentation](https://docs.npmjs.com/files/package.json)
for more information.


## How to Run / Deploy

### Development

To develop locally, run `npm run docs:dev` in the root directory of this repository. This
launches the website at [http://localhost:8080/nskoretz-home/][l-dev]. Changes to your source are
automatically updated.

### Build

To build the /dist folder, run `npm run docs:build` in the root directory of this
repository. By default, the /dist/ folder will be located in /docs/.vuepress/dist/ .
Although, as we have GitLab-CI set up, simply pushing changes to GitLab will trigger the
Runner to relaunch the site on [GitLab Pages][l-glp].


## Footnotes
